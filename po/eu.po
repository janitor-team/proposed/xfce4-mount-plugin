# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Piarres Beobide <pi@beobide.net>, 2006,2008-2009,2016
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-06-08 00:31+0200\n"
"PO-Revision-Date: 2017-09-19 18:06+0000\n"
"Last-Translator: Piarres Beobide <pi@beobide.net>\n"
"Language-Team: Basque (http://www.transifex.com/xfce/xfce-panel-plugins/language/eu/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: eu\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../panel-plugin/devices.c:71
#, c-format
msgid "%.1f B"
msgstr "%.1f B"

#: ../panel-plugin/devices.c:72
#, c-format
msgid "%.1f KB"
msgstr "%.1f KB"

#: ../panel-plugin/devices.c:73
#, c-format
msgid "%.1f MB"
msgstr "%.1f MB"

#: ../panel-plugin/devices.c:74
#, c-format
msgid "%.1f GB"
msgstr "%.1f GB"

#: ../panel-plugin/devices.c:83
#, c-format
msgid "size:                %g\n"
msgstr "tamaina:              %g\n"

#: ../panel-plugin/devices.c:84
#, c-format
msgid "used size:           %g\n"
msgstr "tamaina erabilia:    %g\n"

#: ../panel-plugin/devices.c:85
#, c-format
msgid "available siz:       %g\n"
msgstr "tamaina erabilgarria: %g\n"

#: ../panel-plugin/devices.c:86
#, c-format
msgid "percentage used:     %d\n"
msgstr "ehuneko erabilia:    %d\n"

#: ../panel-plugin/devices.c:87
#, c-format
msgid "file system type:    %s\n"
msgstr "fitxategi sistema mota: %s\n"

#: ../panel-plugin/devices.c:88
#, c-format
msgid "actual mount point:  %s\n"
msgstr "uneko muntatze puntua: %s\n"

#: ../panel-plugin/devices.c:172
#, c-format
msgid "disk: %s\n"
msgstr "diskoa: %s\n"

#: ../panel-plugin/devices.c:173
#, c-format
msgid "mount point: %s\n"
msgstr "muntatze puntua: %s\n"

#: ../panel-plugin/devices.c:177
#, c-format
msgid "not mounted\n"
msgstr "ez muntaturik\n"

#: ../panel-plugin/devices.c:293 ../panel-plugin/devices.c:316
#: ../panel-plugin/devices.c:379 ../panel-plugin/devices.c:390
#: ../panel-plugin/devices.c:400 ../panel-plugin/devices.c:487
msgid "Xfce 4 Mount Plugin"
msgstr "Xfce 4 Muntatze plugina"

#: ../panel-plugin/devices.c:295
msgid "Failed to mount device:"
msgstr "Huts gailua muntatzean:"

#: ../panel-plugin/devices.c:318
msgid "Error executing on-mount command:"
msgstr "Errorea muntatze komandoa abiaraztean:"

#: ../panel-plugin/devices.c:381
msgid "Failed to umount device:"
msgstr "Huts gailua desmuntatzean:"

#: ../panel-plugin/devices.c:392
msgid "The device should be removable safely now:"
msgstr "Gailua orain ziurtasunez deskonektatu daiteke."

#: ../panel-plugin/devices.c:402
msgid "An error occurred. The device should not be removed:"
msgstr "Errore bat gertatu da.  Gailua ez zen kendu beharko:"

#: ../panel-plugin/devices.c:489
msgid ""
"Your /etc/fstab could not be read. This will severely degrade the plugin's "
"abilities."
msgstr "Ezin da /etc/fstab fitxategia irakurri, honek pluginaren gaitasunak asko murrizten ditu."

#: ../panel-plugin/mount-plugin.c:177
msgid " -> "
msgstr " -> "

#: ../panel-plugin/mount-plugin.c:230
#, c-format
msgid "[%s/%s] %s free"
msgstr "[%s/%s] %s libre"

#: ../panel-plugin/mount-plugin.c:249
msgid "<span foreground=\"#FF0000\">not mounted</span>"
msgstr "<span foreground=\"#FF0000\">ez muntaturik</span>"

#: ../panel-plugin/mount-plugin.c:516
msgid "devices"
msgstr "gailuak"

#: ../panel-plugin/mount-plugin.c:707
msgid "Mount Plugin"
msgstr "Muntatze plugina"

#: ../panel-plugin/mount-plugin.c:712
msgid "Properties"
msgstr "Propietateak"

#: ../panel-plugin/mount-plugin.c:743
msgid ""
"This is only useful and recommended if you specify \"sync\" as part of the "
"\"unmount\" command string."
msgstr "Hau erabilgarri eta gomendagarria da \"sync\" erabiltzen baduzu \"unmount\" komandoarekin."

#: ../panel-plugin/mount-plugin.c:747
msgid "Show _message after unmount"
msgstr "Bistarazi _mezua desmuntatu ostean"

#: ../panel-plugin/mount-plugin.c:759
msgid "You can specify a distinct icon to be displayed in the panel."
msgstr "Panelean bistaratzeko ikono ezberdin bat ezarri dezakezu."

#: ../panel-plugin/mount-plugin.c:765
msgid "Icon:"
msgstr "Ikonoa:"

#: ../panel-plugin/mount-plugin.c:769
msgid "Select an image"
msgstr "Irudi bat hautatu"

#: ../panel-plugin/mount-plugin.c:776
msgid "_General"
msgstr "_Orokorra"

#: ../panel-plugin/mount-plugin.c:791
#, c-format
msgid ""
"This command will be executed after mounting the device with the mount point of the device as argument.\n"
"If you are unsure what to insert, try \"exo-open %m\".\n"
"'%d' can be used to specify the device, '%m' for the mountpoint."
msgstr "Komando hau gailua muntatzerakoan honen muntatze puntua argumentu bezala erabiliaz abiaraziko da.\nZer ipini ziur ez badakizu probatu, \"exo-open %m\" erabiliaz.\n'%d' erabili daiteke gailua zehazteko, '%m' muntatze puntuarentzat."

#: ../panel-plugin/mount-plugin.c:800
msgid "_Execute after mounting:"
msgstr "Muntatze ondoren _exekutatu:"

#: ../panel-plugin/mount-plugin.c:823
msgid ""
"WARNING: These options are for experts only! If you do not know what they "
"may be good for, keep your hands off!"
msgstr "ABISUA: Aukera hauek erabiltzaile aurreratuentzat bakarrik dira! Zer ipini ez badakizu mantendu zure eskuak hemendik kanpo!"

#: ../panel-plugin/mount-plugin.c:827
msgid "_Custom commands"
msgstr "Komando _pertsonalizatuak"

#: ../panel-plugin/mount-plugin.c:846
#, c-format
msgid ""
"Most users will only want to prepend \"sudo\" to both commands or prepend \"sync %d &&\" to the \"unmount %d\" command.\n"
"'%d' is used to specify the device, '%m' for the mountpoint."
msgstr "Erabiltzaile gehienek \"sudo\" beharko dute bi komandoentzat, edo \"sync %d &&\"  gehitu aurretik \"unmount %d\" komandoari.\n'%d' erabili daiteke gailua zehazteko, '%m' muntatze puntuarentzat."

#: ../panel-plugin/mount-plugin.c:854
msgid "_Mount command:"
msgstr "_Muntatze komandoa:"

#: ../panel-plugin/mount-plugin.c:859
msgid "_Unmount command:"
msgstr "_Desmuntatze komandoa:"

#: ../panel-plugin/mount-plugin.c:884
msgid "_Commands"
msgstr "_Komandoak"

#: ../panel-plugin/mount-plugin.c:899
msgid ""
"Activate this option to also display network file systems like NFS, SMBFS, "
"SHFS and SSHFS."
msgstr "Aukera hau gaitu sNFS, SMBF, SHFS eta SSHFS antzerko sare bidezko fitxategi sistemak ere bistaratzeko."

#: ../panel-plugin/mount-plugin.c:903
msgid "Display _network file systems"
msgstr "Erakutsi _sare fitxategi-sistemak"

#: ../panel-plugin/mount-plugin.c:916
msgid ""
"Activate this option to also eject a CD-drive after unmounting and to insert"
" before mounting."
msgstr "Aukera hau gaitu CD-gailua desmuntatu ondoren egotzi eta muntatu aurretik ixteko."

#: ../panel-plugin/mount-plugin.c:920
msgid "_Eject CD-drives"
msgstr "CD-gailuak _egotzi"

#: ../panel-plugin/mount-plugin.c:933
msgid "Activate this option to only have the mount points be displayed."
msgstr "Aukera hau gaitu muntatze puntuak bakarrik bistarazirik izateko."

#: ../panel-plugin/mount-plugin.c:936
msgid "Display _mount points only"
msgstr "Erakutsi _muntatze puntuak bakarrik"

#: ../panel-plugin/mount-plugin.c:951
msgid ""
"Trim the device names to the number of characters specified in the spin "
"button."
msgstr "Moztu gailu izenak spin botoian ezarritako karaktere kopuruan."

#: ../panel-plugin/mount-plugin.c:957
msgid "Trim device names: "
msgstr "Moztu gailu izenak:"

#: ../panel-plugin/mount-plugin.c:966
msgid " characters"
msgstr "karaktere"

#: ../panel-plugin/mount-plugin.c:983
msgid ""
"Exclude the following file systems from the menu.\n"
"The list is separated by simple spaces.\n"
"It is up to you to specify correct devices or mount points.\n"
"An asterisk (*) can be used as a placeholder at the end of\n"
"a path, e.g., \"/mnt/*\" to exclude any mountpoints below \"/mnt\".\n"
msgstr ""

#: ../panel-plugin/mount-plugin.c:994
msgid "E_xclude specified file systems"
msgstr "Zehazturiko muntatze puntuak e_z erabili"

#: ../panel-plugin/mount-plugin.c:1009
msgid "_File systems"
msgstr "_Fitxategi sistemak"

#: ../panel-plugin/mount-plugin.c:1034
msgid "Show partitions/devices and allow to mount/unmount them"
msgstr "Ikusi partizio/gailuak eta muntatu/desmuntatzeko aukera eman"

#: ../panel-plugin/mount-plugin.c:1036
msgid "Copyright (c) 2005-2016\n"
msgstr "Copyright (c) 2005-2016\n"

#: ../panel-plugin/xfce4-mount-plugin.desktop.in.h:1
msgid "Mount devices"
msgstr "Gailuak muntatu"

#: ../panel-plugin/xfce4-mount-plugin.desktop.in.h:2
msgid "Shows all mountable devices and (un)mounts them on request."
msgstr "Gailu muntagarri guztiak bistarazi eta eskatzean (des)muntatu."
